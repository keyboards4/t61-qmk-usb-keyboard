EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L jip_connectors:54363-0489 J5
U 1 1 5FEEBBAD
P 2200 4750
F 0 "J5" H 2450 5017 50  0000 C CNN
F 1 "54363-0489" H 2450 4926 50  0000 C CNN
F 2 "jip_connectors:54363-0489" H 2450 2300 50  0001 C CNN
F 3 "~" H 2500 3650 50  0001 C CNN
	1    2200 4750
	1    0    0    -1  
$EndComp
$Comp
L jip_connectors:AA01B-S040VA1 J2
U 1 1 5FEED74A
P 2200 1150
F 0 "J2" H 2450 1417 50  0000 C CNN
F 1 "AA01B-S040VA1" H 2450 1326 50  0000 C CNN
F 2 "jip_connectors:AA01B-S040VA1" H 2450 -1300 50  0001 C CNN
F 3 "~" H 2500 50  50  0001 C CNN
	1    2200 1150
	1    0    0    -1  
$EndComp
NoConn ~ 2700 2350
NoConn ~ 2700 2450
NoConn ~ 2700 2950
NoConn ~ 2700 3050
Wire Wire Line
	2700 3450 2800 3450
Wire Wire Line
	2700 3250 3350 3250
Text Label 3350 3250 2    50   ~ 0
TP4CLK
Text Label 3350 3150 2    50   ~ 0
TP4DATA
Wire Wire Line
	2800 2850 2700 2850
Wire Wire Line
	2800 2850 2800 3450
Wire Wire Line
	2800 2850 2800 1150
Wire Wire Line
	2800 1150 2700 1150
Connection ~ 2800 2850
$Comp
L power:GND #PWR019
U 1 1 5FEFF7C8
P 2800 3650
F 0 "#PWR019" H 2800 3400 50  0001 C CNN
F 1 "GND" H 2805 3477 50  0000 C CNN
F 2 "" H 2800 3650 50  0001 C CNN
F 3 "" H 2800 3650 50  0001 C CNN
	1    2800 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 3650 2800 3450
Connection ~ 2800 3450
$Comp
L power:GND #PWR018
U 1 1 5FF0020B
P 2100 3650
F 0 "#PWR018" H 2100 3400 50  0001 C CNN
F 1 "GND" H 2105 3477 50  0000 C CNN
F 2 "" H 2100 3650 50  0001 C CNN
F 3 "" H 2100 3650 50  0001 C CNN
	1    2100 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3450 2100 3450
Wire Wire Line
	2100 3450 2100 3650
Wire Wire Line
	2100 3450 2100 3050
Wire Wire Line
	2100 3050 2200 3050
Connection ~ 2100 3450
Wire Wire Line
	2200 2950 2100 2950
Wire Wire Line
	2100 2950 2100 3050
Connection ~ 2100 3050
Wire Wire Line
	2200 1150 2100 1150
Wire Wire Line
	2100 1150 2100 2950
Connection ~ 2100 2950
Wire Wire Line
	2200 3250 1550 3250
Text Label 1550 3250 0    50   ~ 0
TP4_RESET
NoConn ~ 2700 2750
NoConn ~ 2700 2650
NoConn ~ 2700 2550
Wire Wire Line
	2700 2250 3350 2250
Text Label 3350 2250 2    50   ~ 0
PWRSWITCH
Wire Wire Line
	2700 1350 3350 1350
Text Label 3350 1350 2    50   ~ 0
HOTKEY
Wire Wire Line
	2700 1450 3350 1450
Wire Wire Line
	2700 1550 3350 1550
Wire Wire Line
	2700 1650 3350 1650
Wire Wire Line
	2700 1750 3350 1750
Wire Wire Line
	2700 1850 3350 1850
Wire Wire Line
	2700 1950 3350 1950
Wire Wire Line
	2700 2050 3350 2050
Wire Wire Line
	2700 2150 3350 2150
Text Label 3350 1450 2    50   ~ 0
SENSE5
Text Label 3350 1550 2    50   ~ 0
SENSE0
Text Label 3350 1650 2    50   ~ 0
SENSE3
Text Label 3350 1750 2    50   ~ 0
SENSE2
Text Label 3350 1850 2    50   ~ 0
SENSE4
Text Label 3350 1950 2    50   ~ 0
SENSE1
Text Label 3350 2050 2    50   ~ 0
SENSE6
Text Label 3350 2150 2    50   ~ 0
SENSE7
Wire Wire Line
	2200 3150 1150 3150
$Comp
L jip_power:P5V #PWR013
U 1 1 5FF078EC
P 1000 3150
F 0 "#PWR013" H 1000 3000 50  0001 C CNN
F 1 "P5V" V 1015 3278 50  0000 L CNN
F 2 "" H 1000 3150 50  0001 C CNN
F 3 "" H 1000 3150 50  0001 C CNN
	1    1000 3150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2200 1350 1550 1350
Text Label 1550 1350 0    50   ~ 0
DRV4
Wire Wire Line
	2200 1450 1550 1450
Wire Wire Line
	2200 1550 1550 1550
Wire Wire Line
	2200 1650 1550 1650
Wire Wire Line
	2200 1750 1550 1750
Wire Wire Line
	2200 1850 1550 1850
Wire Wire Line
	2200 1950 1550 1950
Wire Wire Line
	2200 2050 1550 2050
Wire Wire Line
	2200 2150 1550 2150
Wire Wire Line
	2200 2250 1550 2250
Wire Wire Line
	2200 2350 1550 2350
Wire Wire Line
	2200 2450 1550 2450
Wire Wire Line
	2200 2550 1550 2550
Wire Wire Line
	2200 2650 1550 2650
Wire Wire Line
	2200 2750 1550 2750
Wire Wire Line
	2200 2850 1550 2850
Text Label 1550 1450 0    50   ~ 0
DRV5
Text Label 1550 1550 0    50   ~ 0
DRV8
Text Label 1550 1650 0    50   ~ 0
DRV6
Text Label 1550 1750 0    50   ~ 0
DRV3
Text Label 1550 1850 0    50   ~ 0
DRV7
Text Label 1550 1950 0    50   ~ 0
DRV2
Text Label 1550 2050 0    50   ~ 0
DRV10
Text Label 1550 2150 0    50   ~ 0
DRV1
Text Label 1550 2250 0    50   ~ 0
DRV9
Text Label 1550 2350 0    50   ~ 0
DRV0
Text Label 1550 2450 0    50   ~ 0
DRV11
Text Label 1550 2550 0    50   ~ 0
DRV14
Text Label 1550 2650 0    50   ~ 0
DRV12
Text Label 1550 2750 0    50   ~ 0
DRV15
Text Label 1550 2850 0    50   ~ 0
DRV13
$Comp
L jip_passives:C_custom_fields C5
U 1 1 5FF15195
P 1150 3400
F 0 "C5" H 1035 3263 50  0000 R CNN
F 1 "100n" H 1035 3354 50  0000 R CNN
F 2 "jip_passives:C0603R" H 1188 3250 50  0001 C CNN
F 3 "~" H 1150 3400 50  0001 C CNN
F 4 "25V" H 1035 3445 50  0000 R CNN "Voltage rating"
F 5 "X7R" H 1035 3536 50  0000 R CNN "Dielectrics"
F 6 "0603" H 1035 3582 50  0001 R CNN "Package"
	1    1150 3400
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5FF17469
P 1150 3650
F 0 "#PWR017" H 1150 3400 50  0001 C CNN
F 1 "GND" H 1155 3477 50  0000 C CNN
F 2 "" H 1150 3650 50  0001 C CNN
F 3 "" H 1150 3650 50  0001 C CNN
	1    1150 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 3650 1150 3550
Wire Wire Line
	1150 3250 1150 3150
Connection ~ 1150 3150
Wire Wire Line
	1150 3150 1000 3150
Wire Wire Line
	2700 7050 2800 7050
Wire Wire Line
	2700 6850 3350 6850
Text Label 3350 6850 2    50   ~ 0
TP4CLK
Wire Wire Line
	2700 6750 3350 6750
Text Label 3350 6750 2    50   ~ 0
TP4DATA
Wire Wire Line
	2800 6450 2700 6450
Wire Wire Line
	2800 6450 2800 7050
Wire Wire Line
	2800 6450 2800 4750
Wire Wire Line
	2800 4750 2700 4750
Connection ~ 2800 6450
$Comp
L power:GND #PWR029
U 1 1 5FF24CE0
P 2800 7250
F 0 "#PWR029" H 2800 7000 50  0001 C CNN
F 1 "GND" H 2805 7077 50  0000 C CNN
F 2 "" H 2800 7250 50  0001 C CNN
F 3 "" H 2800 7250 50  0001 C CNN
	1    2800 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 7250 2800 7050
Connection ~ 2800 7050
Wire Wire Line
	2700 5850 3350 5850
Text Label 3350 5850 2    50   ~ 0
PWRSWITCH
Wire Wire Line
	2700 4950 3350 4950
Text Label 3350 4950 2    50   ~ 0
HOTKEY
Wire Wire Line
	2700 5050 3350 5050
Wire Wire Line
	2700 5150 3350 5150
Wire Wire Line
	2700 5250 3350 5250
Wire Wire Line
	2700 5350 3350 5350
Wire Wire Line
	2700 5450 3350 5450
Wire Wire Line
	2700 5550 3350 5550
Wire Wire Line
	2700 5650 3350 5650
Wire Wire Line
	2700 5750 3350 5750
Text Label 3350 5050 2    50   ~ 0
SENSE5
Text Label 3350 5150 2    50   ~ 0
SENSE0
Text Label 3350 5250 2    50   ~ 0
SENSE3
Text Label 3350 5350 2    50   ~ 0
SENSE2
Text Label 3350 5450 2    50   ~ 0
SENSE4
Text Label 3350 5550 2    50   ~ 0
SENSE1
Text Label 3350 5650 2    50   ~ 0
SENSE6
Text Label 3350 5750 2    50   ~ 0
SENSE7
NoConn ~ 2700 5950
NoConn ~ 2700 6050
NoConn ~ 2700 6150
NoConn ~ 2700 6250
NoConn ~ 2700 6350
NoConn ~ 2700 6550
NoConn ~ 2700 6650
$Comp
L power:GND #PWR028
U 1 1 5FF347CB
P 2100 7250
F 0 "#PWR028" H 2100 7000 50  0001 C CNN
F 1 "GND" H 2105 7077 50  0000 C CNN
F 2 "" H 2100 7250 50  0001 C CNN
F 3 "" H 2100 7250 50  0001 C CNN
	1    2100 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 7050 2100 7050
Wire Wire Line
	2100 7050 2100 7250
Wire Wire Line
	2100 7050 2100 6650
Wire Wire Line
	2100 6650 2200 6650
Connection ~ 2100 7050
Wire Wire Line
	2200 6550 2100 6550
Wire Wire Line
	2100 6550 2100 6650
Connection ~ 2100 6650
Wire Wire Line
	2200 4750 2100 4750
Wire Wire Line
	2100 4750 2100 6550
Connection ~ 2100 6550
Wire Wire Line
	2200 6850 1550 6850
Text Label 1550 6850 0    50   ~ 0
TP4_RESET
Wire Wire Line
	2200 6750 1550 6750
Wire Wire Line
	2200 4950 1550 4950
Text Label 1550 4950 0    50   ~ 0
DRV4
Wire Wire Line
	2200 5050 1550 5050
Wire Wire Line
	2200 5150 1550 5150
Wire Wire Line
	2200 5250 1550 5250
Wire Wire Line
	2200 5350 1550 5350
Wire Wire Line
	2200 5450 1550 5450
Wire Wire Line
	2200 5550 1550 5550
Wire Wire Line
	2200 5650 1550 5650
Wire Wire Line
	2200 5750 1550 5750
Wire Wire Line
	2200 5850 1550 5850
Wire Wire Line
	2200 5950 1550 5950
Wire Wire Line
	2200 6050 1550 6050
Wire Wire Line
	2200 6150 1550 6150
Wire Wire Line
	2200 6250 1550 6250
Wire Wire Line
	2200 6350 1550 6350
Wire Wire Line
	2200 6450 1550 6450
Text Label 1550 5050 0    50   ~ 0
DRV5
Text Label 1550 5150 0    50   ~ 0
DRV8
Text Label 1550 5250 0    50   ~ 0
DRV6
Text Label 1550 5350 0    50   ~ 0
DRV3
Text Label 1550 5450 0    50   ~ 0
DRV7
Text Label 1550 5550 0    50   ~ 0
DRV2
Text Label 1550 5650 0    50   ~ 0
DRV10
Text Label 1550 5750 0    50   ~ 0
DRV1
Text Label 1550 5850 0    50   ~ 0
DRV9
Text Label 1550 5950 0    50   ~ 0
DRV0
Text Label 1550 6050 0    50   ~ 0
DRV11
Text Label 1550 6150 0    50   ~ 0
DRV14
Text Label 1550 6250 0    50   ~ 0
DRV12
Text Label 1550 6350 0    50   ~ 0
DRV15
Text Label 1550 6450 0    50   ~ 0
DRV13
$Comp
L jip_power:P5V #PWR027
U 1 1 5FF3C71B
P 1550 6750
F 0 "#PWR027" H 1550 6600 50  0001 C CNN
F 1 "P5V" V 1565 6878 50  0000 L CNN
F 2 "" H 1550 6750 50  0001 C CNN
F 3 "" H 1550 6750 50  0001 C CNN
	1    1550 6750
	0    -1   -1   0   
$EndComp
Wire Notes Line
	3500 750  3500 3950
Wire Notes Line
	3500 3950 650  3950
Wire Notes Line
	650  3950 650  750 
Wire Notes Line
	650  750  3500 750 
Text Notes 650  750  0    50   ~ 0
Original IBM/Lenovo JAE connector
Wire Notes Line
	3500 4350 3500 7550
Wire Notes Line
	3500 7550 650  7550
Wire Notes Line
	650  7550 650  4350
Wire Notes Line
	650  4350 3500 4350
Text Notes 650  4350 0    50   ~ 0
Compatible Molex connector (Digikey WM6787CT-ND)
Text Notes 700  4550 0    100  ~ 0
ASSEMBLY VARIANT
Wire Wire Line
	2700 3150 3350 3150
$Comp
L jip_microchip:AT90USB1286-AU U1
U 1 1 5FEF23B5
P 6600 1300
F 0 "U1" H 7600 1700 50  0000 C CNN
F 1 "AT90USB1286-AU" H 7850 1600 50  0000 C CNN
F 2 "jip_generic_ic:TQFP-64_14x14mm_P0.8mm" H 6550 1300 50  0001 C CNN
F 3 "" H 6550 1300 50  0001 C CNN
	1    6600 1300
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:Crystal_GND24 Y1
U 1 1 5FF12FDD
P 5300 1450
F 0 "Y1" V 5150 1500 50  0000 L CNN
F 1 "FA-238-16MHz" V 5500 1500 50  0000 L CNN
F 2 "jip_misc:FA-238" H 5300 1450 50  0001 C CNN
F 3 "~" H 5300 1450 50  0001 C CNN
	1    5300 1450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5FF2116E
P 4600 1650
F 0 "#PWR05" H 4600 1400 50  0001 C CNN
F 1 "GND" H 4605 1477 50  0000 C CNN
F 2 "" H 4600 1650 50  0001 C CNN
F 3 "" H 4600 1650 50  0001 C CNN
	1    4600 1650
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:C_custom_fields C3
U 1 1 5FF2A53E
P 5050 1700
F 0 "C3" V 5210 1700 50  0000 C CNN
F 1 "12p" V 5301 1700 50  0000 C CNN
F 2 "jip_passives:C0603R" H 5088 1550 50  0001 C CNN
F 3 "~" H 5050 1700 50  0001 C CNN
F 4 "25V" V 5392 1700 50  0000 C CNN "Voltage rating"
F 5 "COG" V 5483 1700 50  0000 C CNN "Dielectrics"
F 6 "0603" V 5574 1700 50  0001 C CNN "Package"
	1    5050 1700
	0    1    1    0   
$EndComp
$Comp
L jip_passives:C_custom_fields C2
U 1 1 5FF331A3
P 5050 1200
F 0 "C2" V 4616 1200 50  0000 C CNN
F 1 "12p" V 4707 1200 50  0000 C CNN
F 2 "jip_passives:C0603R" H 5088 1050 50  0001 C CNN
F 3 "~" H 5050 1200 50  0001 C CNN
F 4 "25V" V 4798 1200 50  0000 C CNN "Voltage rating"
F 5 "COG" V 4889 1200 50  0000 C CNN "Dielectrics"
F 6 "0603" V 4890 1200 50  0001 C CNN "Package"
	1    5050 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	5200 1700 5300 1700
Wire Wire Line
	5300 1700 5300 1600
Wire Wire Line
	5300 1200 5300 1300
Wire Wire Line
	5200 1200 5300 1200
Wire Wire Line
	4800 1450 4800 1200
Wire Wire Line
	4800 1200 4900 1200
Wire Wire Line
	4900 1700 4800 1700
Wire Wire Line
	4800 1700 4800 1450
Wire Wire Line
	4800 1450 5100 1450
Connection ~ 4800 1450
Text Notes 650  7700 0    50   ~ 0
All resistors and capacitors are in the 0603 package unless otherwise noted.
Wire Wire Line
	6600 1500 6000 1500
Wire Wire Line
	6000 1500 6000 1200
Wire Wire Line
	6000 1200 5300 1200
Connection ~ 5300 1200
Connection ~ 5300 1700
Wire Wire Line
	4600 1650 4600 1450
Wire Wire Line
	4600 1450 4800 1450
$Comp
L power:GND #PWR04
U 1 1 5FFAE136
P 5600 1450
F 0 "#PWR04" H 5600 1200 50  0001 C CNN
F 1 "GND" V 5600 1300 50  0000 R CNN
F 2 "" H 5600 1450 50  0001 C CNN
F 3 "" H 5600 1450 50  0001 C CNN
	1    5600 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5600 1450 5500 1450
Wire Wire Line
	5300 1700 6600 1700
$Comp
L jip_passives:R-custom-fields R4
U 1 1 5FFD6CBE
P 5700 2300
F 0 "R4" V 5403 2300 50  0000 C CNN
F 1 "22" V 5494 2300 50  0000 C CNN
F 2 "jip_passives:R0603R" V 5630 2300 50  0001 C CNN
F 3 "" H 5700 2300 50  0001 C CNN
F 4 "0603" V 5493 2300 50  0001 C CNN "Package"
F 5 "1%" V 5585 2300 50  0000 C CNN "Tolerance"
	1    5700 2300
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 2300 6600 2300
$Comp
L jip_passives:R-custom-fields R5
U 1 1 5FFDC62B
P 5700 2400
F 0 "R5" V 5815 2400 50  0000 C CNN
F 1 "22" V 5906 2400 50  0000 C CNN
F 2 "jip_passives:R0603R" V 5630 2400 50  0001 C CNN
F 3 "" H 5700 2400 50  0001 C CNN
F 4 "0603" V 5493 2400 50  0001 C CNN "Package"
F 5 "1%" V 5997 2400 50  0000 C CNN "Tolerance"
	1    5700 2400
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5FFF79D5
P 4100 2200
F 0 "TP1" V 4100 2450 50  0000 C CNN
F 1 "TestPoint" V 4204 2272 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_2x3MM" H 4300 2200 50  0001 C CNN
F 3 "~" H 4300 2200 50  0001 C CNN
	1    4100 2200
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5FFF868E
P 4100 2300
F 0 "TP2" V 4100 2550 50  0000 C CNN
F 1 "TestPoint" V 4204 2372 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_2x3MM" H 4300 2300 50  0001 C CNN
F 3 "~" H 4300 2300 50  0001 C CNN
	1    4100 2300
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5FFF88D2
P 4100 2400
F 0 "TP3" V 4100 2650 50  0000 C CNN
F 1 "TestPoint" V 4204 2472 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_2x3MM" H 4300 2400 50  0001 C CNN
F 3 "~" H 4300 2400 50  0001 C CNN
	1    4100 2400
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5FFF8A19
P 4100 2500
F 0 "TP4" V 4100 2750 50  0000 C CNN
F 1 "TestPoint" V 4204 2572 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_2x3MM" H 4300 2500 50  0001 C CNN
F 3 "~" H 4300 2500 50  0001 C CNN
	1    4100 2500
	0    -1   -1   0   
$EndComp
$Comp
L jip_passives:C_custom_fields C4
U 1 1 60000E8C
P 6350 2600
F 0 "C4" V 6190 2600 50  0000 C CNN
F 1 "2.2u" V 6099 2600 50  0000 C CNN
F 2 "jip_passives:C0603R" H 6388 2450 50  0001 C CNN
F 3 "~" H 6350 2600 50  0001 C CNN
F 4 "16V" V 6008 2600 50  0000 C CNN "Voltage rating"
F 5 "X7R" V 5917 2600 50  0000 C CNN "Dielectrics"
F 6 "0603" H 6235 2782 50  0001 R CNN "Package"
F 7 "" V 6350 2600 50  0001 C CNN "Example"
	1    6350 2600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR012
U 1 1 6000271B
P 6100 2650
F 0 "#PWR012" H 6100 2400 50  0001 C CNN
F 1 "GND" H 6105 2477 50  0000 C CNN
F 2 "" H 6100 2650 50  0001 C CNN
F 3 "" H 6100 2650 50  0001 C CNN
	1    6100 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2650 6100 2600
Wire Wire Line
	6100 2600 6200 2600
Wire Wire Line
	6500 2600 6600 2600
$Comp
L power:GND #PWR024
U 1 1 6000F417
P 7100 5150
F 0 "#PWR024" H 7100 4900 50  0001 C CNN
F 1 "GND" H 7105 4977 50  0000 C CNN
F 2 "" H 7100 5150 50  0001 C CNN
F 3 "" H 7100 5150 50  0001 C CNN
	1    7100 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 5000 7100 5100
Wire Wire Line
	7100 5100 7200 5100
Wire Wire Line
	7200 5100 7200 5000
Connection ~ 7100 5100
Wire Wire Line
	7100 5100 7100 5150
Wire Wire Line
	7200 5100 7300 5100
Wire Wire Line
	7300 5100 7300 5000
Connection ~ 7200 5100
Wire Wire Line
	7300 5100 7400 5100
Wire Wire Line
	7400 5100 7400 5000
Connection ~ 7300 5100
$Comp
L power:GND #PWR011
U 1 1 5FEF5D7A
P 4200 2550
F 0 "#PWR011" H 4200 2300 50  0001 C CNN
F 1 "GND" H 4205 2377 50  0000 C CNN
F 2 "" H 4200 2550 50  0001 C CNN
F 3 "" H 4200 2550 50  0001 C CNN
	1    4200 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2550 4200 2500
Wire Wire Line
	4100 2500 4200 2500
Text Notes 6200 2500 0    50   ~ 0
1uF@5V
$Comp
L jip_power:P5V #PWR02
U 1 1 5FEFCD3A
P 7100 850
F 0 "#PWR02" H 7100 700 50  0001 C CNN
F 1 "P5V" H 7115 1023 50  0000 C CNN
F 2 "" H 7100 850 50  0001 C CNN
F 3 "" H 7100 850 50  0001 C CNN
	1    7100 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 850  7100 900 
Wire Wire Line
	7400 1000 7400 900 
Wire Wire Line
	7400 900  7300 900 
Connection ~ 7100 900 
Wire Wire Line
	7100 900  7100 1000
Wire Wire Line
	7300 1000 7300 900 
Connection ~ 7300 900 
Wire Wire Line
	7300 900  7200 900 
Wire Wire Line
	7200 1000 7200 900 
Connection ~ 7200 900 
Wire Wire Line
	7200 900  7100 900 
$Comp
L jip_power:P5V #PWR07
U 1 1 5FF15A1F
P 6450 2100
F 0 "#PWR07" H 6450 1950 50  0001 C CNN
F 1 "P5V" V 6465 2228 50  0000 L CNN
F 2 "" H 6450 2100 50  0001 C CNN
F 3 "" H 6450 2100 50  0001 C CNN
	1    6450 2100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 2400 6600 2400
Wire Wire Line
	4100 2300 5550 2300
Wire Wire Line
	4100 2400 5550 2400
$Comp
L jip_power:P5V #PWR08
U 1 1 5FF64963
P 4600 2150
F 0 "#PWR08" H 4600 2000 50  0001 C CNN
F 1 "P5V" H 4615 2323 50  0000 C CNN
F 2 "" H 4600 2150 50  0001 C CNN
F 3 "" H 4600 2150 50  0001 C CNN
	1    4600 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2200 4100 2200
Wire Wire Line
	6450 2100 6600 2100
NoConn ~ 6600 1900
$Comp
L jip_passives:C_custom_fields C8
U 1 1 5FF78F24
P 4650 6950
F 0 "C8" H 4535 6813 50  0000 R CNN
F 1 "100n" H 4535 6904 50  0000 R CNN
F 2 "jip_passives:C0603R" H 4688 6800 50  0001 C CNN
F 3 "~" H 4650 6950 50  0001 C CNN
F 4 "25V" H 4535 6995 50  0000 R CNN "Voltage rating"
F 5 "X7R" H 4535 7086 50  0000 R CNN "Dielectrics"
F 6 "0603" H 4535 7132 50  0001 R CNN "Package"
	1    4650 6950
	-1   0    0    1   
$EndComp
$Comp
L jip_passives:C_custom_fields C9
U 1 1 5FF793A1
P 5100 6950
F 0 "C9" H 4985 6813 50  0000 R CNN
F 1 "100n" H 4985 6904 50  0000 R CNN
F 2 "jip_passives:C0603R" H 5138 6800 50  0001 C CNN
F 3 "~" H 5100 6950 50  0001 C CNN
F 4 "25V" H 4985 6995 50  0000 R CNN "Voltage rating"
F 5 "X7R" H 4985 7086 50  0000 R CNN "Dielectrics"
F 6 "0603" H 4985 7132 50  0001 R CNN "Package"
	1    5100 6950
	-1   0    0    1   
$EndComp
$Comp
L jip_passives:C_custom_fields C10
U 1 1 5FF7FCD5
P 5550 6950
F 0 "C10" H 5435 6813 50  0000 R CNN
F 1 "10n" H 5435 6904 50  0000 R CNN
F 2 "jip_passives:C0603R" H 5588 6800 50  0001 C CNN
F 3 "~" H 5550 6950 50  0001 C CNN
F 4 "25V" H 5435 6995 50  0000 R CNN "Voltage rating"
F 5 "X7R" H 5435 7086 50  0000 R CNN "Dielectrics"
F 6 "0603" H 5435 7132 50  0001 R CNN "Package"
	1    5550 6950
	-1   0    0    1   
$EndComp
$Comp
L jip_passives:C_custom_fields C11
U 1 1 5FF7FFDC
P 6000 6950
F 0 "C11" H 5885 6813 50  0000 R CNN
F 1 "10n" H 5885 6904 50  0000 R CNN
F 2 "jip_passives:C0603R" H 6038 6800 50  0001 C CNN
F 3 "~" H 6000 6950 50  0001 C CNN
F 4 "25V" H 5885 6995 50  0000 R CNN "Voltage rating"
F 5 "X7R" H 5885 7086 50  0000 R CNN "Dielectrics"
F 6 "0603" H 5885 7132 50  0001 R CNN "Package"
	1    6000 6950
	-1   0    0    1   
$EndComp
$Comp
L jip_passives:C_custom_fields C7
U 1 1 5FF8034B
P 4200 6950
F 0 "C7" H 4315 7132 50  0000 L CNN
F 1 "10u" H 4315 7041 50  0000 L CNN
F 2 "jip_passives:C0805R" H 4238 6800 50  0001 C CNN
F 3 "~" H 4200 6950 50  0001 C CNN
F 4 "25V" H 4315 6950 50  0000 L CNN "Voltage rating"
F 5 "X7R" H 4315 6859 50  0000 L CNN "Dielectrics"
F 6 "0805" H 4315 6768 50  0000 L CNN "Package"
	1    4200 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 7100 4200 7200
Wire Wire Line
	4200 7200 4650 7200
Wire Wire Line
	6000 7200 6000 7100
Wire Wire Line
	5550 7100 5550 7200
Connection ~ 5550 7200
Wire Wire Line
	5550 7200 6000 7200
Wire Wire Line
	5100 7100 5100 7200
Connection ~ 5100 7200
Wire Wire Line
	5100 7200 5550 7200
Wire Wire Line
	4650 7100 4650 7200
Connection ~ 4650 7200
Wire Wire Line
	4650 7200 5100 7200
Wire Wire Line
	4200 6800 4200 6700
Wire Wire Line
	4200 6700 4650 6700
Wire Wire Line
	6000 6700 6000 6800
Wire Wire Line
	5550 6800 5550 6700
Connection ~ 5550 6700
Wire Wire Line
	5550 6700 6000 6700
Wire Wire Line
	5100 6800 5100 6700
Connection ~ 5100 6700
Wire Wire Line
	5100 6700 5550 6700
Wire Wire Line
	4650 6800 4650 6700
Connection ~ 4650 6700
Wire Wire Line
	4650 6700 5100 6700
$Comp
L jip_power:P5V #PWR026
U 1 1 5FFDED82
P 4200 6650
F 0 "#PWR026" H 4200 6500 50  0001 C CNN
F 1 "P5V" H 4215 6823 50  0000 C CNN
F 2 "" H 4200 6650 50  0001 C CNN
F 3 "" H 4200 6650 50  0001 C CNN
	1    4200 6650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR030
U 1 1 5FFDF0B3
P 4200 7250
F 0 "#PWR030" H 4200 7000 50  0001 C CNN
F 1 "GND" H 4205 7077 50  0000 C CNN
F 2 "" H 4200 7250 50  0001 C CNN
F 3 "" H 4200 7250 50  0001 C CNN
	1    4200 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 7250 4200 7200
Connection ~ 4200 7200
Wire Wire Line
	4200 6650 4200 6700
Connection ~ 4200 6700
Wire Notes Line
	4100 2100 4100 2600
Wire Notes Line
	4100 2600 3700 2600
Wire Notes Line
	3700 2600 3700 2100
Wire Notes Line
	3700 2100 4100 2100
Text Notes 3700 2100 0    50   ~ 0
USB PADS
Wire Wire Line
	7800 4200 8450 4200
Text Label 9950 4200 2    50   ~ 0
TP4DATA
Wire Wire Line
	7800 4500 9450 4500
Text Label 9950 4500 2    50   ~ 0
TP4CLK
$Comp
L jip_passives:C_custom_fields C6
U 1 1 60027B00
P 9100 5100
F 0 "C6" H 9215 5237 50  0000 L CNN
F 1 "2.2u" H 9215 5146 50  0000 L CNN
F 2 "jip_passives:C0603R" H 9138 4950 50  0001 C CNN
F 3 "~" H 9100 5100 50  0001 C CNN
F 4 "16V" H 9215 5055 50  0000 L CNN "Voltage rating"
F 5 "X7R" H 9215 4964 50  0000 L CNN "Dielectrics"
F 6 "0603" H 8985 5282 50  0001 R CNN "Package"
F 7 "" V 9100 5100 50  0001 C CNN "Example"
	1    9100 5100
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:R-custom-fields R14
U 1 1 60030781
P 9100 5600
F 0 "R14" H 9170 5691 50  0000 L CNN
F 1 "10k" H 9170 5600 50  0000 L CNN
F 2 "jip_passives:R0603R" V 9030 5600 50  0001 C CNN
F 3 "" H 9100 5600 50  0001 C CNN
F 4 "0603" H 9170 5555 50  0001 L CNN "Package"
F 5 "1%" H 9170 5509 50  0000 L CNN "Tolerance"
	1    9100 5600
	1    0    0    -1  
$EndComp
Wire Notes Line
	4000 7550 6400 7550
Wire Notes Line
	6400 7550 6400 6350
Wire Notes Line
	6400 6350 4000 6350
Wire Notes Line
	4000 6350 4000 7550
Text Notes 4000 6350 0    50   ~ 0
Bypass capacitors
$Comp
L power:GND #PWR025
U 1 1 60042E20
P 9100 5800
F 0 "#PWR025" H 9100 5550 50  0001 C CNN
F 1 "GND" H 9105 5627 50  0000 C CNN
F 2 "" H 9100 5800 50  0001 C CNN
F 3 "" H 9100 5800 50  0001 C CNN
	1    9100 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 5800 9100 5750
Wire Wire Line
	9100 5450 9100 5350
Connection ~ 9100 5350
Wire Wire Line
	9100 5350 9100 5250
$Comp
L jip_power:P5V #PWR022
U 1 1 6006D851
P 9100 4900
F 0 "#PWR022" H 9100 4750 50  0001 C CNN
F 1 "P5V" H 9115 5073 50  0000 C CNN
F 2 "" H 9100 4900 50  0001 C CNN
F 3 "" H 9100 4900 50  0001 C CNN
	1    9100 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 4900 9100 4950
Text Label 9950 5350 2    50   ~ 0
TP4_RESET
Text Notes 7850 4200 0    50   ~ 0
~RXD1
Text Notes 7850 4500 0    50   ~ 0
~XCK1
Text Label 8150 4100 2    50   ~ 0
SDA
Text Label 8150 4000 2    50   ~ 0
SCL
$Comp
L jip_power:P5V #PWR020
U 1 1 600EC9C2
P 10500 4200
F 0 "#PWR020" H 10500 4050 50  0001 C CNN
F 1 "P5V" V 10515 4328 50  0000 L CNN
F 2 "" H 10500 4200 50  0001 C CNN
F 3 "" H 10500 4200 50  0001 C CNN
	1    10500 4200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10500 4200 10600 4200
$Comp
L power:GND #PWR021
U 1 1 60113344
P 10500 4350
F 0 "#PWR021" H 10500 4100 50  0001 C CNN
F 1 "GND" H 10505 4177 50  0000 C CNN
F 2 "" H 10500 4350 50  0001 C CNN
F 3 "" H 10500 4350 50  0001 C CNN
	1    10500 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 4350 10500 4300
Wire Wire Line
	10500 4300 10600 4300
$Comp
L jip_passives:R-custom-fields R2
U 1 1 60131134
P 4350 2200
F 0 "R2" V 4053 2200 50  0000 C CNN
F 1 "PTC0.5A" V 4144 2200 50  0000 C CNN
F 2 "jip_passives:R1206R" V 4280 2200 50  0001 C CNN
F 3 "" H 4350 2200 50  0001 C CNN
F 4 "1206" V 4235 2200 50  0000 C CNN "Package"
F 5 "1%" V 4234 2200 50  0001 C CNN "Tolerance"
	1    4350 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 2200 4600 2200
Wire Wire Line
	4600 2200 4600 2150
$Comp
L jip_passives:R-custom-fields R11
U 1 1 6013B7B4
P 9800 3750
F 0 "R11" H 9870 3841 50  0000 L CNN
F 1 "10k" H 9870 3750 50  0000 L CNN
F 2 "jip_passives:R0603R" V 9730 3750 50  0001 C CNN
F 3 "" H 9800 3750 50  0001 C CNN
F 4 "0603" H 9870 3705 50  0001 L CNN "Package"
F 5 "1%" H 9870 3659 50  0000 L CNN "Tolerance"
	1    9800 3750
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:R-custom-fields R12
U 1 1 6013C032
P 10150 3750
F 0 "R12" H 10220 3841 50  0000 L CNN
F 1 "10k" H 10220 3750 50  0000 L CNN
F 2 "jip_passives:R0603R" V 10080 3750 50  0001 C CNN
F 3 "" H 10150 3750 50  0001 C CNN
F 4 "0603" H 10220 3705 50  0001 L CNN "Package"
F 5 "1%" H 10220 3659 50  0000 L CNN "Tolerance"
	1    10150 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 3900 9800 4000
Wire Wire Line
	10150 3900 10150 4100
$Comp
L jip_power:P5V #PWR015
U 1 1 6014F099
P 10150 3450
F 0 "#PWR015" H 10150 3300 50  0001 C CNN
F 1 "P5V" H 10165 3623 50  0000 C CNN
F 2 "" H 10150 3450 50  0001 C CNN
F 3 "" H 10150 3450 50  0001 C CNN
	1    10150 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 3600 9800 3500
Wire Wire Line
	10150 3500 10150 3450
Wire Wire Line
	10150 3600 10150 3500
Text Notes 10500 3900 0    50   ~ 0
I2C connector
Wire Wire Line
	8150 4400 7800 4400
Wire Wire Line
	8150 4400 8150 5350
Text Notes 7900 5800 0    50   ~ 0
Trackpoint reset circuit with \noptional MCU override
$Comp
L jip_passives:R_Network04 R6
U 1 1 6019B2CC
P 5650 3750
F 0 "R6" H 5720 3796 50  0000 L CNN
F 1 "10k" H 5720 3705 50  0000 L CNN
F 2 "jip_passives:R1206-ARRAY-4" H 5650 3750 50  0001 C CNN
F 3 "" H 5650 3750 50  0001 C CNN
	1    5650 3750
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:R_Network04 R6
U 2 1 6019C413
P 5350 3750
F 0 "R6" H 5420 3796 50  0000 L CNN
F 1 "10k" H 5420 3705 50  0000 L CNN
F 2 "jip_passives:R1206-ARRAY-4" H 5350 3750 50  0001 C CNN
F 3 "" H 5350 3750 50  0001 C CNN
	2    5350 3750
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:R_Network04 R6
U 3 1 6019D05D
P 5050 3750
F 0 "R6" H 5120 3796 50  0000 L CNN
F 1 "10k" H 5120 3705 50  0000 L CNN
F 2 "jip_passives:R1206-ARRAY-4" H 5050 3750 50  0001 C CNN
F 3 "" H 5050 3750 50  0001 C CNN
	3    5050 3750
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:R_Network04 R6
U 4 1 6019DD2E
P 4750 3750
F 0 "R6" H 4820 3796 50  0000 L CNN
F 1 "10k" H 4820 3705 50  0000 L CNN
F 2 "jip_passives:R1206-ARRAY-4" H 4750 3750 50  0001 C CNN
F 3 "" H 4750 3750 50  0001 C CNN
	4    4750 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 4000 4750 4000
Wire Wire Line
	6600 4100 5050 4100
Wire Wire Line
	6600 4300 5650 4300
Wire Wire Line
	4250 4200 5350 4200
Wire Wire Line
	4750 3900 4750 4000
Connection ~ 4750 4000
Wire Wire Line
	4750 4000 6600 4000
Wire Wire Line
	5050 3900 5050 4100
Connection ~ 5050 4100
Wire Wire Line
	5050 4100 4250 4100
Wire Wire Line
	5350 3900 5350 4200
Connection ~ 5350 4200
Wire Wire Line
	5350 4200 6600 4200
Wire Wire Line
	5650 3900 5650 4300
Connection ~ 5650 4300
Wire Wire Line
	5650 4300 4250 4300
$Comp
L jip_power:P5V #PWR014
U 1 1 601FC4A3
P 4750 3450
F 0 "#PWR014" H 4750 3300 50  0001 C CNN
F 1 "P5V" H 4765 3623 50  0000 C CNN
F 2 "" H 4750 3450 50  0001 C CNN
F 3 "" H 4750 3450 50  0001 C CNN
	1    4750 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3600 4750 3500
Wire Wire Line
	4750 3500 5050 3500
Wire Wire Line
	5650 3500 5650 3600
Wire Wire Line
	5350 3600 5350 3500
Connection ~ 5350 3500
Wire Wire Line
	5350 3500 5650 3500
Wire Wire Line
	5050 3600 5050 3500
Connection ~ 5050 3500
Wire Wire Line
	5050 3500 5350 3500
Wire Wire Line
	4750 3450 4750 3500
Connection ~ 4750 3500
Wire Wire Line
	6600 4400 4750 4400
Wire Wire Line
	6600 4500 5050 4500
Wire Wire Line
	6600 4600 5350 4600
Wire Wire Line
	6600 4700 5650 4700
$Comp
L jip_passives:R_Network04 R13
U 1 1 60251B69
P 4750 4950
F 0 "R13" H 4820 4996 50  0000 L CNN
F 1 "10k" H 4820 4905 50  0000 L CNN
F 2 "jip_passives:R1206-ARRAY-4" H 4750 4950 50  0001 C CNN
F 3 "" H 4750 4950 50  0001 C CNN
	1    4750 4950
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:R_Network04 R13
U 2 1 60251B73
P 5050 4950
F 0 "R13" H 5120 4996 50  0000 L CNN
F 1 "10k" H 5120 4905 50  0000 L CNN
F 2 "jip_passives:R1206-ARRAY-4" H 5050 4950 50  0001 C CNN
F 3 "" H 5050 4950 50  0001 C CNN
	2    5050 4950
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:R_Network04 R13
U 3 1 60251B7D
P 5350 4950
F 0 "R13" H 5420 4996 50  0000 L CNN
F 1 "10k" H 5420 4905 50  0000 L CNN
F 2 "jip_passives:R1206-ARRAY-4" H 5350 4950 50  0001 C CNN
F 3 "" H 5350 4950 50  0001 C CNN
	3    5350 4950
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:R_Network04 R13
U 4 1 60251B87
P 5650 4950
F 0 "R13" H 5720 4996 50  0000 L CNN
F 1 "10k" H 5720 4905 50  0000 L CNN
F 2 "jip_passives:R1206-ARRAY-4" H 5650 4950 50  0001 C CNN
F 3 "" H 5650 4950 50  0001 C CNN
	4    5650 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 4400 4750 4800
Connection ~ 4750 4400
Wire Wire Line
	4750 4400 4250 4400
Connection ~ 5050 4500
Wire Wire Line
	5050 4500 4250 4500
Wire Wire Line
	5350 4800 5350 4600
Connection ~ 5350 4600
Wire Wire Line
	5350 4600 4250 4600
Wire Wire Line
	5650 4800 5650 4700
Connection ~ 5650 4700
Wire Wire Line
	5650 4700 4250 4700
Wire Wire Line
	5050 4500 5050 4800
Wire Wire Line
	4750 5100 4750 5200
Wire Wire Line
	4750 5200 5050 5200
Wire Wire Line
	6100 5200 6100 5150
Wire Wire Line
	5650 5100 5650 5200
Connection ~ 5650 5200
Wire Wire Line
	5650 5200 6100 5200
Wire Wire Line
	5350 5100 5350 5200
Connection ~ 5350 5200
Wire Wire Line
	5350 5200 5650 5200
Wire Wire Line
	5050 5100 5050 5200
Connection ~ 5050 5200
Wire Wire Line
	5050 5200 5350 5200
$Comp
L jip_power:P5V #PWR023
U 1 1 602D6EA1
P 6100 5150
F 0 "#PWR023" H 6100 5000 50  0001 C CNN
F 1 "P5V" H 6115 5323 50  0000 C CNN
F 2 "" H 6100 5150 50  0001 C CNN
F 3 "" H 6100 5150 50  0001 C CNN
	1    6100 5150
	1    0    0    -1  
$EndComp
Text Label 4250 4700 0    50   ~ 0
SENSE5
Text Label 4250 4600 0    50   ~ 0
SENSE0
Text Label 4250 4500 0    50   ~ 0
SENSE3
Text Label 4250 4400 0    50   ~ 0
SENSE2
Text Label 4250 4300 0    50   ~ 0
SENSE4
Text Label 4250 4200 0    50   ~ 0
SENSE1
Text Label 4250 4100 0    50   ~ 0
SENSE6
Text Label 4250 4000 0    50   ~ 0
SENSE7
$Comp
L jip_power:P5V #PWR010
U 1 1 60371ABC
P 10550 2350
F 0 "#PWR010" H 10550 2200 50  0001 C CNN
F 1 "P5V" H 10565 2523 50  0000 C CNN
F 2 "" H 10550 2350 50  0001 C CNN
F 3 "" H 10550 2350 50  0001 C CNN
	1    10550 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 2350 10550 2400
Wire Wire Line
	10550 2600 10600 2600
$Comp
L Device:Q_DUAL_NPN_PNP_BRT_E1B1C2E2B2C1 Q1
U 2 1 603935C7
P 10500 3150
F 0 "Q1" H 10641 3196 50  0000 L CNN
F 1 "PUMH10" H 10641 3105 50  0000 L CNN
F 2 "jip_generic_ic:SOT-363" H 10500 3150 50  0001 C CNN
F 3 "~" H 10500 3150 50  0001 C CNN
	2    10500 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_DUAL_NPN_PNP_BRT_E1B1C2E2B2C1 Q1
U 1 1 603949A6
P 10500 1550
F 0 "Q1" H 10640 1596 50  0000 L CNN
F 1 "PUMH10" H 10640 1505 50  0000 L CNN
F 2 "jip_generic_ic:SOT-363" H 10500 1550 50  0001 C CNN
F 3 "~" H 10500 1550 50  0001 C CNN
	1    10500 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 2950 10550 2900
Wire Wire Line
	10550 2700 10600 2700
$Comp
L power:GND #PWR016
U 1 1 603B3BB4
P 10550 3450
F 0 "#PWR016" H 10550 3200 50  0001 C CNN
F 1 "GND" H 10555 3277 50  0000 C CNN
F 2 "" H 10550 3450 50  0001 C CNN
F 3 "" H 10550 3450 50  0001 C CNN
	1    10550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 3350 10550 3450
$Comp
L jip_passives:R-custom-fields R1
U 1 1 603F4BFA
P 6750 900
F 0 "R1" V 7047 900 50  0000 C CNN
F 1 "10k" V 6956 900 50  0000 C CNN
F 2 "jip_passives:R0603R" V 6680 900 50  0001 C CNN
F 3 "" H 6750 900 50  0001 C CNN
F 4 "0603" H 6820 855 50  0001 L CNN "Package"
F 5 "1%" V 6865 900 50  0000 C CNN "Tolerance"
	1    6750 900 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6900 900  7100 900 
Wire Wire Line
	6500 1300 6500 1000
Wire Wire Line
	6500 900  6600 900 
Wire Wire Line
	6500 1300 6600 1300
$Comp
L jip_passives:C_custom_fields C1
U 1 1 60447A84
P 6250 1000
F 0 "C1" V 5816 1000 50  0000 C CNN
F 1 "100n" V 5907 1000 50  0000 C CNN
F 2 "jip_passives:C0603R" H 6288 850 50  0001 C CNN
F 3 "~" H 6250 1000 50  0001 C CNN
F 4 "25V" V 5998 1000 50  0000 C CNN "Voltage rating"
F 5 "X7R" V 6089 1000 50  0000 C CNN "Dielectrics"
F 6 "0603" H 6135 1182 50  0001 R CNN "Package"
	1    6250 1000
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 1000 6500 1000
Connection ~ 6500 1000
Wire Wire Line
	6500 1000 6500 900 
$Comp
L power:GND #PWR03
U 1 1 60457379
P 6050 1000
F 0 "#PWR03" H 6050 750 50  0001 C CNN
F 1 "GND" V 6055 872 50  0000 R CNN
F 2 "" H 6050 1000 50  0001 C CNN
F 3 "" H 6050 1000 50  0001 C CNN
	1    6050 1000
	0    1    1    0   
$EndComp
Wire Wire Line
	6050 1000 6100 1000
Wire Wire Line
	8450 1300 7800 1300
Wire Wire Line
	8450 1400 7800 1400
Wire Wire Line
	8450 1500 7800 1500
Wire Wire Line
	7800 1600 8450 1600
Wire Wire Line
	8450 1700 7800 1700
Wire Wire Line
	7800 1800 8450 1800
Wire Wire Line
	8450 1900 7800 1900
Wire Wire Line
	8450 2000 7800 2000
$Comp
L Device:D_Schottky D3
U 1 1 6051C368
P 10150 2650
F 0 "D3" V 10104 2730 50  0000 L CNN
F 1 "#" V 10195 2730 50  0000 L CNN
F 2 "jip_passives:R0603R" H 10150 2650 50  0001 C CNN
F 3 "~" H 10150 2650 50  0001 C CNN
	1    10150 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	10150 2800 10150 2900
Wire Wire Line
	10150 2900 10550 2900
Connection ~ 10550 2900
Wire Wire Line
	10550 2900 10550 2700
Wire Wire Line
	10150 2500 10150 2400
Wire Wire Line
	10150 2400 10550 2400
Connection ~ 10550 2400
Wire Wire Line
	10550 2400 10550 2600
Text Notes 10950 2700 0    50   ~ 0
AUX2
Wire Wire Line
	8150 3800 7800 3800
Wire Wire Line
	7800 3700 8150 3700
Wire Wire Line
	8150 3600 7800 3600
Wire Wire Line
	7800 3500 8150 3500
Wire Wire Line
	7800 3400 8150 3400
Wire Wire Line
	7800 3200 8150 3200
Wire Wire Line
	7800 3300 8150 3300
Wire Wire Line
	7800 3100 8150 3100
Text Label 8450 1300 2    50   ~ 0
DRV4
Text Label 8450 1400 2    50   ~ 0
DRV5
Text Label 8450 1500 2    50   ~ 0
DRV8
Text Label 8450 1600 2    50   ~ 0
DRV6
Text Label 8450 1800 2    50   ~ 0
DRV7
Text Label 8450 1900 2    50   ~ 0
DRV2
Text Label 8450 2000 2    50   ~ 0
DRV10
Text Label 8150 3800 2    50   ~ 0
DRV1
Text Label 8150 3700 2    50   ~ 0
DRV9
Text Label 8150 3600 2    50   ~ 0
DRV0
Text Label 8150 3500 2    50   ~ 0
DRV11
Text Label 8150 3400 2    50   ~ 0
DRV14
Text Label 8150 3300 2    50   ~ 0
DRV12
Text Label 8150 3200 2    50   ~ 0
DRV15
Text Label 8150 3100 2    50   ~ 0
DRV13
Text Label 8450 1700 2    50   ~ 0
DRV3
$Comp
L jip_power:P5V #PWR01
U 1 1 6065414A
P 10550 750
F 0 "#PWR01" H 10550 600 50  0001 C CNN
F 1 "P5V" H 10565 923 50  0000 C CNN
F 2 "" H 10550 750 50  0001 C CNN
F 3 "" H 10550 750 50  0001 C CNN
	1    10550 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 750  10550 800 
Wire Wire Line
	10550 1000 10600 1000
Wire Wire Line
	10550 1350 10550 1300
Wire Wire Line
	10550 1100 10600 1100
$Comp
L power:GND #PWR06
U 1 1 60654162
P 10550 1850
F 0 "#PWR06" H 10550 1600 50  0001 C CNN
F 1 "GND" H 10555 1677 50  0000 C CNN
F 2 "" H 10550 1850 50  0001 C CNN
F 3 "" H 10550 1850 50  0001 C CNN
	1    10550 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 1750 10550 1850
$Comp
L Device:D_Schottky D1
U 1 1 6065416D
P 10150 1050
F 0 "D1" V 10104 1130 50  0000 L CNN
F 1 "#" V 10195 1130 50  0000 L CNN
F 2 "jip_passives:R0603R" H 10150 1050 50  0001 C CNN
F 3 "~" H 10150 1050 50  0001 C CNN
	1    10150 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	10150 1200 10150 1300
Wire Wire Line
	10150 1300 10550 1300
Connection ~ 10550 1300
Wire Wire Line
	10550 1300 10550 1100
Wire Wire Line
	10150 900  10150 800 
Wire Wire Line
	10150 800  10550 800 
Connection ~ 10550 800 
Wire Wire Line
	10550 800  10550 1000
Text Notes 10950 1100 0    50   ~ 0
AUX1
Wire Wire Line
	7800 2900 9900 2900
Wire Wire Line
	9900 2900 9900 3150
Wire Wire Line
	9900 3150 10150 3150
Wire Wire Line
	9900 1550 9900 2800
Wire Wire Line
	9900 2800 7800 2800
Wire Wire Line
	9900 1550 10150 1550
Text Label 9950 4300 2    50   ~ 0
HOTKEY
$Comp
L jip_passives:R-custom-fields R8
U 1 1 6078F06C
P 8750 3750
F 0 "R8" H 8820 3841 50  0000 L CNN
F 1 "10k" H 8820 3750 50  0000 L CNN
F 2 "jip_passives:R0603R" V 8680 3750 50  0001 C CNN
F 3 "" H 8750 3750 50  0001 C CNN
F 4 "0603" H 8820 3705 50  0001 L CNN "Package"
F 5 "1%" H 8820 3659 50  0000 L CNN "Tolerance"
	1    8750 3750
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:R-custom-fields R9
U 1 1 6078F482
P 9100 3750
F 0 "R9" H 9170 3841 50  0000 L CNN
F 1 "10k" H 9170 3750 50  0000 L CNN
F 2 "jip_passives:R0603R" V 9030 3750 50  0001 C CNN
F 3 "" H 9100 3750 50  0001 C CNN
F 4 "0603" H 9170 3705 50  0001 L CNN "Package"
F 5 "1%" H 9170 3659 50  0000 L CNN "Tolerance"
	1    9100 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 4300 8750 3900
Wire Wire Line
	7800 4300 8750 4300
Wire Wire Line
	8750 3600 8750 3500
Wire Wire Line
	8750 4300 9950 4300
Connection ~ 8750 4300
Wire Wire Line
	7800 4600 9100 4600
Wire Wire Line
	9100 4600 9100 3900
Wire Wire Line
	9100 3600 9100 3500
Text Label 9950 4600 2    50   ~ 0
PWRSWITCH
$Comp
L jip_passives:R-custom-fields R3
U 1 1 6083E558
P 8700 2200
F 0 "R3" V 8403 2200 50  0000 C CNN
F 1 "560" V 8494 2200 50  0000 C CNN
F 2 "jip_passives:R0603R" V 8630 2200 50  0001 C CNN
F 3 "" H 8700 2200 50  0001 C CNN
F 4 "0603" V 8493 2200 50  0001 C CNN "Package"
F 5 "1%" V 8585 2200 50  0000 C CNN "Tolerance"
	1    8700 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	8550 2200 7800 2200
Wire Wire Line
	8850 2200 8950 2200
$Comp
L power:GND #PWR09
U 1 1 6087D15C
P 9400 2200
F 0 "#PWR09" H 9400 1950 50  0001 C CNN
F 1 "GND" V 9405 2072 50  0000 R CNN
F 2 "" H 9400 2200 50  0001 C CNN
F 3 "" H 9400 2200 50  0001 C CNN
	1    9400 2200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9400 2200 9250 2200
$Comp
L jip_passives:R-custom-fields R7
U 1 1 6094DEEC
P 8450 3750
F 0 "R7" H 8520 3841 50  0000 L CNN
F 1 "4k7" H 8520 3750 50  0000 L CNN
F 2 "jip_passives:R0603R" V 8380 3750 50  0001 C CNN
F 3 "" H 8450 3750 50  0001 C CNN
F 4 "0603" H 8520 3705 50  0001 L CNN "Package"
F 5 "1%" H 8520 3659 50  0000 L CNN "Tolerance"
	1    8450 3750
	1    0    0    -1  
$EndComp
$Comp
L jip_passives:R-custom-fields R10
U 1 1 6094EE8A
P 9450 3750
F 0 "R10" H 9520 3841 50  0000 L CNN
F 1 "4k7" H 9520 3750 50  0000 L CNN
F 2 "jip_passives:R0603R" V 9380 3750 50  0001 C CNN
F 3 "" H 9450 3750 50  0001 C CNN
F 4 "0603" H 9520 3705 50  0001 L CNN "Package"
F 5 "1%" H 9520 3659 50  0000 L CNN "Tolerance"
	1    9450 3750
	1    0    0    -1  
$EndComp
Connection ~ 8750 3500
Connection ~ 9100 4600
Wire Wire Line
	9100 4600 9950 4600
Wire Wire Line
	9800 3500 10150 3500
Connection ~ 9800 3500
Wire Wire Line
	8750 3500 9100 3500
Connection ~ 10150 3500
Wire Wire Line
	8450 3600 8450 3500
Wire Wire Line
	8450 3500 8750 3500
Wire Wire Line
	8450 3900 8450 4200
Connection ~ 8450 4200
Wire Wire Line
	8450 4200 9950 4200
Connection ~ 9800 4000
Wire Wire Line
	9800 4000 10600 4000
Connection ~ 10150 4100
Wire Wire Line
	10150 4100 10600 4100
Wire Wire Line
	7800 4000 9800 4000
Wire Wire Line
	7800 4100 10150 4100
Wire Wire Line
	9100 3500 9450 3500
Connection ~ 9100 3500
Wire Wire Line
	9450 3600 9450 3500
Connection ~ 9450 3500
Wire Wire Line
	9450 3500 9800 3500
Wire Wire Line
	9450 3900 9450 4500
Connection ~ 9450 4500
Wire Wire Line
	9450 4500 9950 4500
Wire Wire Line
	8150 5350 9100 5350
Wire Wire Line
	9100 5350 9950 5350
$Comp
L Connector:TestPoint TP5
U 1 1 5FF18851
P 10600 1000
F 0 "TP5" V 10600 1250 50  0000 C CNN
F 1 "TestPoint" V 10704 1072 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_0.8x2.3MM" H 10800 1000 50  0001 C CNN
F 3 "~" H 10800 1000 50  0001 C CNN
	1    10600 1000
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP6
U 1 1 5FF1932D
P 10600 1100
F 0 "TP6" V 10600 1350 50  0000 C CNN
F 1 "TestPoint" V 10704 1172 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_0.8x2.3MM" H 10800 1100 50  0001 C CNN
F 3 "~" H 10800 1100 50  0001 C CNN
	1    10600 1100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP7
U 1 1 5FF19613
P 10600 2600
F 0 "TP7" V 10600 2850 50  0000 C CNN
F 1 "TestPoint" V 10704 2672 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_0.8x2.3MM" H 10800 2600 50  0001 C CNN
F 3 "~" H 10800 2600 50  0001 C CNN
	1    10600 2600
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP8
U 1 1 5FF19BBF
P 10600 2700
F 0 "TP8" V 10600 2950 50  0000 C CNN
F 1 "TestPoint" V 10704 2772 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_0.8x2.3MM" H 10800 2700 50  0001 C CNN
F 3 "~" H 10800 2700 50  0001 C CNN
	1    10600 2700
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP9
U 1 1 5FF19EAB
P 10600 4000
F 0 "TP9" V 10600 4250 50  0000 C CNN
F 1 "TestPoint" V 10704 4072 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_0.8x2.3MM" H 10800 4000 50  0001 C CNN
F 3 "~" H 10800 4000 50  0001 C CNN
	1    10600 4000
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP10
U 1 1 5FF1A1EB
P 10600 4100
F 0 "TP10" V 10600 4350 50  0000 C CNN
F 1 "TestPoint" V 10704 4172 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_0.8x2.3MM" H 10800 4100 50  0001 C CNN
F 3 "~" H 10800 4100 50  0001 C CNN
	1    10600 4100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP11
U 1 1 5FF1A387
P 10600 4200
F 0 "TP11" V 10600 4450 50  0000 C CNN
F 1 "TestPoint" V 10704 4272 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_0.8x2.3MM" H 10800 4200 50  0001 C CNN
F 3 "~" H 10800 4200 50  0001 C CNN
	1    10600 4200
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP12
U 1 1 5FF1A507
P 10600 4300
F 0 "TP12" V 10600 4550 50  0000 C CNN
F 1 "TestPoint" V 10704 4372 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_0.8x2.3MM" H 10800 4300 50  0001 C CNN
F 3 "~" H 10800 4300 50  0001 C CNN
	1    10600 4300
	0    1    1    0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5FF219C8
P 9100 2200
F 0 "D2" H 9093 1945 50  0000 C CNN
F 1 "LED" H 9093 2036 50  0000 C CNN
F 2 "jip_passives:LED0603R" H 9100 2200 50  0001 C CNN
F 3 "~" H 9100 2200 50  0001 C CNN
	1    9100 2200
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP13
U 1 1 5FF8986E
P 9350 2400
F 0 "TP13" V 9350 2650 50  0000 C CNN
F 1 "TestPoint" V 9454 2472 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_0.8x2.3MM" H 9550 2400 50  0001 C CNN
F 3 "~" H 9550 2400 50  0001 C CNN
	1    9350 2400
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP14
U 1 1 5FF89F2D
P 9350 2500
F 0 "TP14" V 9350 2750 50  0000 C CNN
F 1 "TestPoint" V 9454 2572 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_0.8x2.3MM" H 9550 2500 50  0001 C CNN
F 3 "~" H 9550 2500 50  0001 C CNN
	1    9350 2500
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP15
U 1 1 5FF8A1D9
P 9350 2600
F 0 "TP15" V 9350 2850 50  0000 C CNN
F 1 "TestPoint" V 9454 2672 50  0001 C CNN
F 2 "jip_misc:SMD_PAD_0.8x2.3MM" H 9550 2600 50  0001 C CNN
F 3 "~" H 9550 2600 50  0001 C CNN
	1    9350 2600
	0    1    1    0   
$EndComp
$Comp
L jip_passives:R-custom-fields R15
U 1 1 5FFC963D
P 8700 2500
F 0 "R15" V 8850 2500 50  0000 C CNN
F 1 "560" V 8950 2500 50  0000 C CNN
F 2 "jip_passives:R0603R" V 8630 2500 50  0001 C CNN
F 3 "" H 8700 2500 50  0001 C CNN
F 4 "0603" V 8493 2500 50  0001 C CNN "Package"
F 5 "1%" V 8700 2500 50  0000 C CNN "Tolerance"
	1    8700 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	8550 2500 7800 2500
Wire Wire Line
	8850 2500 9350 2500
$Comp
L jip_power:P5V #PWR031
U 1 1 5FFF3CAA
P 9300 2400
F 0 "#PWR031" H 9300 2250 50  0001 C CNN
F 1 "P5V" V 9315 2528 50  0000 L CNN
F 2 "" H 9300 2400 50  0001 C CNN
F 3 "" H 9300 2400 50  0001 C CNN
	1    9300 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9300 2400 9350 2400
$Comp
L power:GND #PWR032
U 1 1 600099B5
P 9300 2600
F 0 "#PWR032" H 9300 2350 50  0001 C CNN
F 1 "GND" V 9305 2472 50  0000 R CNN
F 2 "" H 9300 2600 50  0001 C CNN
F 3 "" H 9300 2600 50  0001 C CNN
	1    9300 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	9300 2600 9350 2600
Text Notes 9450 2750 0    50   ~ 0
AUX3
$EndSCHEMATC
